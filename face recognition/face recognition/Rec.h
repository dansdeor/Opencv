#pragma once

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/face.hpp"
#include "opencv2/core/core.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;
using namespace cv;

void read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, char separator = ';') {
	std::ifstream file(filename.c_str(), ifstream::in);
	if (!file) {
		string error_message = "No valid input file was given, please check the given filename.";
		CV_Error(CV_StsBadArg, error_message);
	}
	string line, path, classlabel;
	while (getline(file, line)) {
		stringstream liness(line);
		getline(liness, path, separator);
		getline(liness, classlabel);
		if (!path.empty() && !classlabel.empty()) {
			images.push_back(imread(path, 0));
			labels.push_back(atoi(classlabel.c_str()));
		}
	}
}

void FaceTrainer()
{
	vector<Mat> images;
	vector<int> labels;

	images.push_back(imread("1/1.jpg", CV_LOAD_IMAGE_GRAYSCALE)); labels.push_back(0);
	images.push_back(imread("1/2.jpg", CV_LOAD_IMAGE_GRAYSCALE)); labels.push_back(0);
	images.push_back(imread("1/3.jpg", CV_LOAD_IMAGE_GRAYSCALE)); labels.push_back(0);
	images.push_back(imread("1/4.jpg", CV_LOAD_IMAGE_GRAYSCALE)); labels.push_back(0);
	images.push_back(imread("1/5.jpg", CV_LOAD_IMAGE_GRAYSCALE)); labels.push_back(0);

	Ptr<face::FaceRecognizer> model = face::createEigenFaceRecognizer();

	try
	{
		cout << "Start the training" << endl;
		model->train(images, labels);
		cout << "End the training" << endl;
		model->save("dataset.yml");
	}
	catch (Exception e)
	{
		cout << e.msg << endl;
	}
}
void FaceRecognizer()
{
	int label;
	string name;
	double confidence;

	CascadeClassifier classifier;
	classifier.load("haarcascade_frontalface_default.xml");

	Ptr<face::FaceRecognizer> model = face::createEigenFaceRecognizer();
	model->load("dataset.yml");

	VideoCapture captureDevice;
	captureDevice.open(0);

	Mat frame;
	Mat gray_scale;

	//namedWindow("Face detection for now", 1);

	while (true)
	{
		captureDevice >> frame;

		cvtColor(frame, gray_scale, CV_BGR2GRAY);
		equalizeHist(gray_scale, gray_scale);

		vector<Rect> faces; //                                 | CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_SCALE_IMAGE
		classifier.detectMultiScale(gray_scale, faces, 1.1, 3, 0, Size(30, 30));

		for (int i = 0; i < faces.size(); i++)
		{
			Mat face(gray_scale, faces[i]);//take the rectangle from the gray image
			resize(face, face, Size(170, 170), 1.0, 1.0, INTER_CUBIC);//this will prevent from the recognizer to throw exception
			try {
				model->predict(face, label, confidence);
			}catch(...){}
			if (label == 0)
				name = "Dan Sde-Or , confidence: "+to_string(confidence);
			else
				name = "Unknown";
			//Point p1(faces[i].x + faces[i].width, faces[i].y + faces[i].height);
			//Point p2(faces[i].x, faces[i].y);
			rectangle(frame, faces[i], cvScalar(50, 255, 0, 0), 1, 8, 0);
			putText(frame, name, Point(faces[i].x - 10, faces[i].y - 10), FONT_HERSHEY_COMPLEX_SMALL, 0.6, cvScalar(50, 255, 0, 0));
		}
		imshow("Face recognition", frame);
		if (waitKey(1) == 'q')
			break;
	}
}

void PicturesTaker(int num)
{
	CascadeClassifier classifier;
	classifier.load("haarcascade_frontalface_default.xml");

	VideoCapture captureDevice;
	captureDevice.open(0);

	Mat frame;
	Mat gray_scale;

	//namedWindow("Face detection for now", 1);

	while (num>0)
	{
		
		captureDevice >> frame;

		cvtColor(frame, gray_scale, CV_BGR2GRAY);
		equalizeHist(gray_scale, gray_scale);

		vector<Rect> faces; //                                | CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_SCALE_IMAGE
		classifier.detectMultiScale(gray_scale, faces, 1.1, 3, 0, Size(30, 30));

		for (int i = 0; i < faces.size(); i++)
		{
			//Point p1(faces[i].x + faces[i].width, faces[i].y + faces[i].height);
			//Point p2(faces[i].x, faces[i].y);	
			if (waitKey(1) == ' ')
			{
				Mat face(frame, faces[i]);
				resize(face, face, Size(170, 170));//this will prevent from the recognizer to throw exception
				imwrite("1/" + to_string(num) + ".jpg", face);
				num--;
			}

			rectangle(frame, faces[i], cvScalar(50, 255, 0, 0), 3, 8, 0);
		}
		imshow("Face recognition", frame);
		if (waitKey(1) == 'q')
			break;
	}
}